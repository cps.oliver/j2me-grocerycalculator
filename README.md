J2ME-GroceryCalculator
------

This project is a simple J2ME demo application for feature phones like the Nokia 220, Nokia 3310 (2017), or any other device that allows the running of J2ME applications.

This project uses the following technology:

+ Java 1.7 (targeting the 1.3 version used for J2ME)
+ Sun Wireless Toolkit 2.5.2 
+ LWUIT 1.5 (UI Framework)
+ FreeJ2ME (Emulator)
+ Netbeans 7.2 (Development IDE)

All versions of the technologies used have either been chosen at their most recent version, or the versions that provide required functionality which was latter dropped.

This application has been tested on the following phones:

+ Nokia 3310 3G (2017, Model TA-1022)

## Screenshot

This is the current version of the application running in the FreeJ2ME emulator as of August 2019.

![Running example of the application](/images/screenshot.png)

## Useful Info

This blog post proved invaluable to me when getting a J2ME development environment setup.

https://rohanverma.net/blog/2017/02/07/working-with-j2me-on-linux-in-2017/

## Notes

This project should build for you as long as you've setup all the dependencies and prerequisites. It is worth noting however that I made a change to the build-impl.xml file to automatically start the FreeJ2ME emulator with the newest version of the application on the `post-jar` step.

You can update this as needed, but these were my changes:

```
<target name="post-jar">
	<exec executable="/bin/sh">
		<arg value="-c"/>
		<arg value="java -jar /home/connor/j2me/freej2me/build/freej2me.jar file:${dist.dir}/${dist.jar}" />
	</exec>
</target>
```

## License

This application is licensed under the GPLv3. See `LICENSE` for more details.

## Credit

This app was written by Connor Oliver, connor@muezza.ca.
