/*
 * This file is part of J2ME-GroceryCalculator.
 *
 * J2ME-GroceryCalculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * J2ME-GroceryCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with J2ME-GroceryCalculator.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Writen By: Connor Oliver, connor@muezza.ca, 2019
 */
package ca.muezza.core;

import ca.muezza.utils.NumberUtils;

public class RunningTally {
    private float[][] tallyHistory = new float[500][2];
    private float total = 0.0f;
    
    public RunningTally(){
        
    }
    
    public void addEntry(float qty, float price){
        this.tallyHistory[this.tallyHistory.length - 1][0] = qty;
        this.tallyHistory[this.tallyHistory.length - 1][1] = price;
        this.total += price * qty;
    }
    
    public float getTotal(){
        return this.total;
    }
    
    public String getTotalString(){
        return NumberUtils.floatToMoneyString(this.total);
    }
    
    public float[] getLastEntry(){
        return this.tallyHistory[this.tallyHistory.length - 1];
    }
    
    public String getLastEntryString(){
        float[] lastEntry = this.tallyHistory[this.tallyHistory.length - 1];
        
        return (int) lastEntry[0] + " * " + NumberUtils.floatToMoneyString(lastEntry[1]);
    }
    
    public float[][] getTallyHistory(){
        return this.tallyHistory;
    }
}
