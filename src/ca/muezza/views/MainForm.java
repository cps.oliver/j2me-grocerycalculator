/*
 * This file is part of J2ME-GroceryCalculator.
 *
 * J2ME-GroceryCalculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * J2ME-GroceryCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with J2ME-GroceryCalculator.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Writen By: Connor Oliver, connor@muezza.ca, 2019
 */
package ca.muezza.views;

import ca.muezza.constants.Colours;
import ca.muezza.constants.Phone;
import ca.muezza.core.RunningTally;
import ca.muezza.views.inf.IForm;
import com.sun.lwuit.Button;
import com.sun.lwuit.Form;
import com.sun.lwuit.Label;
import com.sun.lwuit.List;
import com.sun.lwuit.TextArea;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.layouts.CoordinateLayout;
import com.sun.lwuit.plaf.Border;

public class MainForm implements IForm {
    private static final String TITLE = "Grocery Calculator";
    private Form mainForm;
    private RunningTally runningTally;
    private List itemList;
    private Label itemListLabel, totalInputLabel, qtyInputLabel, priceInputLabel;
    private TextArea qtyInput, priceInput, totalInput;
    private Button addButton;
    
    public MainForm(RunningTally runningTally){
        this.mainForm = new Form(TITLE);
        this.runningTally = runningTally;
        
        this.buildForm();
    }
    
    public void loadExistingDataIntoForm(){
        // Not implemented yet
    }
    
    public void show(){
        mainForm.show();
    }
    
    public Form getForm(){
        return mainForm;
    }
    
    private void buildForm(){
        // Set the MainForm to use a CorodLayout
        mainForm.setLayout(new CoordinateLayout(Phone.SCREEN_WIDTH, Phone.SCREEN_HEIGHT));
        
        // Item List Label
        itemListLabel = new Label("Item List");
        itemListLabel.setX(1);
        itemListLabel.setY(1);
        itemListLabel.getUnselectedStyle().setBgTransparency(100);
        itemListLabel.getUnselectedStyle().setBgColor(Colours.GREY);
        
        // Item list
        itemList = new List();
        itemList.setX(1); 
        itemList.setY(20);
        itemList.setPreferredW(236);
        itemList.setPreferredH(166);
        itemList.setFocusable(false);
        itemList.getStyle().setBorder(Border.createLineBorder(1));
        itemList.getUnselectedStyle().setBgTransparency(100);
        itemList.getUnselectedStyle().setBgColor(Colours.LIGHT_BLUE);
        
        // Total Input Label
        totalInputLabel = new Label("Total");
        totalInputLabel.setX(1);
        totalInputLabel.setY(201);
        totalInputLabel.getUnselectedStyle().setBgTransparency(100);
        totalInputLabel.getUnselectedStyle().setBgColor(Colours.GREY);
        
        // Total Input
        totalInput = new TextArea(1, 20, TextArea.NUMERIC);
        totalInput.setX(1);
        totalInput.setY(220);
        totalInput.setPreferredW(236);
        totalInput.setEditable(false);
        totalInput.setFocusable(false);
        totalInput.setText("$0.00");
        
        // Quantity Input Label
        qtyInputLabel = new Label("Quantity");
        qtyInputLabel.setX(1);
        qtyInputLabel.setY(246);
        qtyInputLabel.getUnselectedStyle().setBgTransparency(100);
        qtyInputLabel.getUnselectedStyle().setBgColor(Colours.GREY);
        
        // Quantity Input
        qtyInput = new TextArea(1, 2, TextArea.NUMERIC);
        qtyInput.setX(1);
        qtyInput.setY(265);
        qtyInput.setPreferredW(85);
        qtyInput.setFocusable(true);
        qtyInput.setFocus(true);
        qtyInput.getSelectedStyle().setBgColor(Colours.LIGHT_RED);
        
        // Price Input Label
        priceInputLabel = new Label("Cost");
        priceInputLabel.setX(91);
        priceInputLabel.setY(246);
        priceInputLabel.getUnselectedStyle().setBgTransparency(100);
        priceInputLabel.getUnselectedStyle().setBgColor(Colours.GREY);
        
        // Price Input
        priceInput = new TextArea(1, 10, TextArea.DECIMAL);
        priceInput.setX(91);
        priceInput.setY(265);
        priceInput.setPreferredW(146);
        priceInput.setFocusable(true);
        priceInput.getSelectedStyle().setBgColor(Colours.LIGHT_RED);
        
        // Add Button
        addButton = new Button("Add");
        addButton.setX(1);
        addButton.setY(295);
        addButton.setPreferredW(236);
        addButton.setFocusable(true);
        addButton.getSelectedStyle().setBgColor(Colours.LIGHT_RED);
        addButton.addActionListener(this.addButtonAction());
        
        // Set focus orders
        priceInput.setNextFocusLeft(qtyInput);
        priceInput.setNextFocusDown(addButton);
        qtyInput.setNextFocusRight(priceInput);
        addButton.setNextFocusUp(qtyInput);
       
        // Add Components to MainForm
        mainForm.addComponent(itemListLabel);
        mainForm.addComponent(itemList);
        mainForm.addComponent(totalInputLabel);
        mainForm.addComponent(totalInput);
        mainForm.addComponent(qtyInputLabel);
        mainForm.addComponent(qtyInput);
        mainForm.addComponent(priceInputLabel);
        mainForm.addComponent(priceInput);
        mainForm.addComponent(addButton);
    }
    
    private ActionListener addButtonAction(){
        return new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                float qty = Float.parseFloat(qtyInput.getText());
                float totalCost = Float.parseFloat(priceInput.getText());

                runningTally.addEntry(qty, totalCost);

                itemList.addItem(runningTally.getLastEntryString());
                itemList.setSelectedIndex(itemList.size() -1);
                
                totalInput.setText(runningTally.getTotalString());
                qtyInput.setText("");
                priceInput.setText("");
                
                qtyInput.requestFocus();
            }
        };
    }
}
