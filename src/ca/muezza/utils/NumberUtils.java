/*
 * This file is part of J2ME-GroceryCalculator.
 *
 * J2ME-GroceryCalculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * J2ME-GroceryCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with J2ME-GroceryCalculator.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Writen By: Connor Oliver, connor@muezza.ca, 2019
 */
package ca.muezza.utils;

public class NumberUtils {
    private static String MONEY_PREFIX = "$";
    private static String ZERO_SUFFIX = "0";
    
    public static String floatToMoneyString(float number){
        String numString = String.valueOf(number);
        char[] numChars = numString.toCharArray();
        
        for (int i = 0; i < numChars.length; i++) {
            if(numChars[i] == '.'){
                if(numChars.length - (i + 1) == 2){
                    numString = MONEY_PREFIX + String.valueOf(numChars);
                    break;
                } else if (numChars.length - (i + 1) > 2){
                    // We set the cut position as i+3 because we're at the decimal
                    // in 3.001, so we need to get it and the two elements after
                    numString = MONEY_PREFIX + String.valueOf(ArrayUtils.cutCharArray(numChars, (i + 3)));
                    break;
                } else {
                    numString = MONEY_PREFIX + String.valueOf(numChars) + ZERO_SUFFIX;
                    break;
                }
            }
        }
        
        return numString;
    }
    
}
