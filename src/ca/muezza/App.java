/*
 * This file is part of J2ME-GroceryCalculator.
 *
 * J2ME-GroceryCalculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * J2ME-GroceryCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with J2ME-GroceryCalculator.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Writen By: Connor Oliver, connor@muezza.ca, 2019
 */
package ca.muezza;

import ca.muezza.core.RunningTally;
import ca.muezza.views.MainForm;
import com.sun.lwuit.Display;
import javax.microedition.midlet.*;

public class App extends MIDlet {
    private boolean started = false;
    private MainForm mainForm;
    private RunningTally runningTally;
    

    public App() {
       Display.init(this);
    }

    protected void startApp() {
        // When the phone screen turns off that counts as a pause event,
        // when we wake the phone up start gets called again. That's
        // a problem as we don't want to redraw the screen, so we set
        // a flag and check if we already started once before.
        if(!started){
            this.runningTally = new RunningTally();
            this.mainForm = new MainForm(this.runningTally);

            this.mainForm.show();
            
            this.started = true;
        }
    }

    protected void pauseApp() {
        
    }

    protected void destroyApp(boolean unconditional) {
        notifyDestroyed();
    }
}