/*
 * This file is part of J2ME-GroceryCalculator.
 *
 * J2ME-GroceryCalculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * J2ME-GroceryCalculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with J2ME-GroceryCalculator.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Writen By: Connor Oliver, connor@muezza.ca, 2019
 */
package ca.muezza.constants;

public class Phone {
    public static int SCREEN_HEIGHT = 320;
    public static int SCREEN_WIDTH = 240;
}
